#!/bin/bash
export SECRET="test-secret";
export PGUSER="postgres";
export PGDATABASE="leaderboard";

ARRAY="$@"

for f in `ls tests/hookfiles`; do
  CODE=$(echo $f | cut -d'.' -f 1)

  for i in $ARRAY ; do
    if [ $i -eq $CODE ] ; then  
      echo "Test -> $f"
      echo "abao apidoc/api.raml --sorted --hookfiles \"tests/hookfiles/$f\" --server \"http://api:8081/api/admin\" --reporter list"
    
      if output=$(abao apidoc/api.raml --sorted --hookfiles "tests/hookfiles/$f" --server "http://api:8081/api/admin" --reporter list); then
        echo "$output";
        echo "SUCCESS!";
      else
        status=$?;
        echo "$output";
        echo "FAILED!";
        exit $status;
      fi
    fi
  done

done
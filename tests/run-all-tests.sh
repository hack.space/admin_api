#!/bin/bash
export SECRET="test-secret";
export PGUSER="postgres";
export PGDATABASE="leaderboard";

for f in `ls tests/hookfiles`; do
  echo "Test -> $f"
  echo "abao apidoc/api.raml --sorted --hookfiles \"tests/hookfiles/$f\" --server \"http://api:8081/api/admin\" --reporter list"

  if output=$(abao apidoc/api.raml --sorted --hookfiles "tests/hookfiles/$f" --server "http://api:8081/api/admin" --reporter list); then
    echo "$output";
    echo "SUCCESS!";
  else
    status=$?;
    echo "$output";
    echo "FAILED!";
    exit $status;
  fi

done

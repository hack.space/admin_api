# ABAO Tests

## Command to run the tests

* run all tests  
`sh tests/run-all-tests.sh`

* run specific tests  
`sh tests/run-tests.sh 101 102 201`

## How to generate test files

`abao ../apidoc/api.raml --generate-hooks > hookfiles/basic-tests.js`

## Useful links

[ABAO Github documentation](https://github.com/cybertk/abao)

## Run a single hookfile

`abao ../apidoc/api.raml --sorted --server http://localhost:8081/api --hookfiles test-basic.js`

"Sorted" puts the requests in a good way to test. [Link](https://github.com/cybertk/abao#command-line-options)

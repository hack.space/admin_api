//
// ABAO hooks file 
// Generated from RAML specification
//   RAML: api.raml
//   Date: 2018-06-25 13:07:51
// <https://github.com/cybertk/abao>
//
const hooks = require('hooks');
const { assert } = require('chai');
const actorEsb = require('../../src/ESBs/actors.esb');

//
// Setup/Teardown
//
let objectId = "";
let payload = {
  username: "test-user",
  role: "admin",
  pwd: "12345678",
  email: "test@test.com"
};

hooks.beforeAll(function (done) {
  actorEsb.create(payload, 1)
    .then(res => {
      objectId = res.id;
      console.log("Test actor was created with id: ", objectId);
      done();
    })
    .catch(err => {
      if (err.code == 409) {
        console.log("Test actor is already exists");
        done();
      } else {
        console.error("Error while creating test actor:", err);
        process.exit(-1);
      }
    });
});

hooks.afterAll(function (done) {
  // done(); 
});

// Hooks

// -----------------------------------------------------------------------------
hooks.before('POST /login -> 200', function (test, done) {
  test.request.body = {
    login: payload.email,
    pwd: payload.pwd
  };
  done();
});

hooks.after('POST /login -> 200', function (test, done) {
  // let resBody = test.response.body;
  // console.log(test);
  // assert(resBody, "no body in response")
  // assert(resBody.token, "no token field in response");
  done();
});

// -----------------------------------------------------------------------------
hooks.before('POST /login -> 401', function (test, done) {
  console.log(test);
  test.request.body = {
    login: payload.email,
    pwd: 'wrong password'
  };
  done();
});

hooks.after('POST /login -> 401', function (test, done) {
  done();
});

hooks.skip('GET /actors -> 200');
hooks.skip('POST /actors -> 200');
hooks.skip('POST /actors -> 400');
hooks.skip('POST /actors -> 409');
hooks.skip('GET /actors/{id} -> 200');
hooks.skip('PUT /actors/{id} -> 200');
hooks.skip('PUT /actors/{id} -> 400');
hooks.skip('DELETE /actors/{id} -> 200');
// hooks.skip('POST /login -> 200');
hooks.skip('POST /login -> 401');
hooks.skip('GET /messages -> 200');
hooks.skip('POST /messages -> 200');
hooks.skip('POST /messages -> 400');
hooks.skip('GET /participants/{id} -> 200');
//
// ABAO hooks file 
// Generated from RAML specification
//   RAML: api.raml
//   Date: 2018-06-26 06:40:42
// <https://github.com/cybertk/abao>
//
const hooks = require('hooks');
const { assert } = require('chai');
//
// Setup/Teardown
//
let objectId = "";

hooks.beforeAll(function (done) {
  done();
});

hooks.afterAll(function (done) {
  done();
});

// Hooks

//-----------------------------------------------------------------------------
hooks.before('GET /actors -> 200', function (test, done) {
  // Modify 'test.request' properties here to modify the inbound request
  done();
});

hooks.after('GET /actors -> 200', function (test, done) {
  // Assert against 'test.response' properties here to verify expected results
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /actors -> 200', function (test, done) {
  done();
});

hooks.after('POST /actors -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /actors -> 400', function (test, done) {
  done();
});

hooks.after('POST /actors -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('POST /actors -> 409', function (test, done) {
  done();
});

hooks.after('POST /actors -> 409', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('GET /actors/{id} -> 200', function (test, done) {
  done();
});

hooks.after('GET /actors/{id} -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('PUT /actors/{id} -> 200', function (test, done) {
  done();
});

hooks.after('PUT /actors/{id} -> 200', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('PUT /actors/{id} -> 400', function (test, done) {
  done();
});

hooks.after('PUT /actors/{id} -> 400', function (test, done) {
  done();
});

//-----------------------------------------------------------------------------
hooks.before('DELETE /actors/{id} -> 200', function (test, done) {
  done();
});

hooks.after('DELETE /actors/{id} -> 200', function (test, done) {
  done();
});


// hooks.skip('GET /actors -> 200');
// hooks.skip('POST /actors -> 200');
// hooks.skip('POST /actors -> 400');
// hooks.skip('POST /actors -> 409');
hooks.skip('GET /actors/{id} -> 200');
hooks.skip('PUT /actors/{id} -> 200');
hooks.skip('PUT /actors/{id} -> 400');
hooks.skip('DELETE /actors/{id} -> 200');
hooks.skip('POST /login -> 200');
hooks.skip('POST /login -> 401');
hooks.skip('GET /messages -> 200');
hooks.skip('POST /messages -> 200');
hooks.skip('POST /messages -> 400');
hooks.skip('GET /participants/{id} -> 200');


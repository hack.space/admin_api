# References:

- `/apidoc` - general folder of api design
- `/apidoc/api.raml` - main documentation for end-points. If you face some problems with it, ask Roman
- `/apidoc/examples` - contains examples of body in responses and requests for end-points provided in raml doc
- `/apidoc/schemas` - contains schemas of JSON objects which should be sent or received in requests.

## Environment variables
- `TG_BOT_TOKEN` (required)
- `BOT_URL` (required)
- `SECRET` (required)
- `PGUSER` (optional, default: postgres)
- `PGDATABASE` (optional, default: postgres)
- `PGPASSWORD`
- `EMAIL_USER`
- `EMAIL_PWD`

# Login and Authorization
1. Organizator fill in form
2. We create organizations and admin user in Database
3. Admin user creates other secondary users of the organization

| role  | what can do                 |
|-------|-----------------------------|
| admin | CRUD actors + manage events |
| basic | manage events               |

# Events management
1. Actor create a draft
2. Publish it
3. Anounce in calendar

**Restrictions for MVP^**
| Operation | Draft | Anounce | Comment |
|-----------|-------|---------|---------|
| Create    | +     | +       |
| Read      | +     | +       |
| Update    | +     | -       | Notification for users is required
| Delete    | +     | -       | Reason of removing from organizator is required

# Messaging

1. POST /messages will send a message to all participants
2. In current implementation the main chanel is Telegram, so it will be send to the bot.
3. Messages are stored in the database for keeping history for organizators


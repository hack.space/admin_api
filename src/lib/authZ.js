let unit = {};

const ROLES = [
  'admin',
  'basic',
  'judge'
]

/**
 * @param req._acl.role - role of an authenticated user
 */
async function authorize(req, res, next) {
  try {
    let role = req._acl.role;

    if (ROLES.indexOf(role) === -1)
      return res.status(403).send({ message: 'access denied' });

    // TODO: use map or db instead
    // if role is admin or request just for reading
    if (/^\/api\/admin\/actors(\/\d+)?$/.exec(req.baseUrl)) {
      if (req.method === 'GET' || role === 'admin')
        return next();
    } else if (/^\/api\/admin\/verdicts(\/\d+|\/list)?$/.exec(req.originalUrl)) {
      if (req.method === 'GET' || role === 'judge')
        return next();
    } else if (/^\/api\/admin\/verdicts\/\d+\/comments$/.exec(req.originalUrl)) {
      if (role === 'basic' || role === 'admin')
        return next();
    } else if ((/^\/api\/admin\/teams(\/\d+)?$/.exec(req.baseUrl))) {
      if (req.method === 'GET' && role === 'judge') {
        req.query.eventId = req._acl.eventId;
        return next();
      } else if (role === 'admin' || role === 'basic')
        return next();
    } else if (role === 'admin' || role === 'basic')
      return next(); // if not /actors is requested

    return res.status(403).send({ message: 'access denied' });
  } catch (err) {
    return res.status(500).send({ message: err.message })
  }
}

unit.middleware = () => {
  return authorize;
}

module.exports = unit;
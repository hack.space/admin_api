const { google } = require('googleapis');
const fs = require('fs');
const SCOPES = [
  'https://www.googleapis.com/auth/spreadsheets'
];

async function readCredentials () {
  return new Promise((res, rej) => {
    fs.readFile('credentials.json', 'utf8', (err, content) => {
      try {
        return res(JSON.parse(content));
      } catch (err) {
        return rej(err);
      }
    });
  })
}

async function oauth2Factory () {
  const creds = await readCredentials();
  const { client_secret, client_id, redirect_uris } = creds.installed;
  const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);
  return oAuth2Client;
}

async function getLoginUrl() {
  try {
    const oAuth2Client = await oauth2Factory();
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    return authUrl;
  } catch (err) {
    console.log(err);
    return "";
  }
}

async function getNewToken(code) {
  const oAuth2Client = await oauth2Factory();
  const {tokens} = await oAuth2Client.getToken(code);
  return tokens;
}

async function authorize(token) {
  const oAuth2Client = await oauth2Factory();
  oAuth2Client.setCredentials(token);
  return oAuth2Client;
}

module.exports = {
  authorize,
  getLoginUrl,
  getNewToken,
}
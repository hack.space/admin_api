const esb = require('../../../ESBs/verdicts.esb')
const verdictsDb = require('../../../controllers/verdicts.controller');
const judgeEsb = require('../../../ESBs/judges.esb');

const raw = (...args) => ({
  values: Object.values(args).map(el => {
    if (typeof el === 'object') {
      console.log('ERR! EL is OBJECT', JSON.stringify(el));
      el = 'X%';
    }
    return {
      userEnteredValue: {
        [`${typeof el}Value`]: el
      }
    }
  })
})

const getData = async (eventId, orgId) => {
  // VERDICTS
  const appView = await esb.getList({ eventId }, orgId);
  if (appView.length === 0)
    return raw();

  const verdictsTableView = [raw('Команда', 'Средний балл', ...appView[0].criterias.map(c => c.name))];
  verdictsTableView.push(
    appView.map(el => raw(
      el.team.name,
      el.summary.averageWeighted,
      ...el.criterias.map(c => c.average)
    ))
  );

  // COMMENTS
  const commentsTableView = [raw('Жюри', 'Команда', 'Комментарий')];
  for (const v of appView) {
    console.debug(v.team.name);
    try {
      const comments = await esb.getCommentsByTeam(v.team.id);
      console.debug(comments);
      commentsTableView.push(...comments.map(c => raw(c.judgeName, v.team.name, c.comment)));
    } catch (err) {
      console.log(`No comments for team ${v.team.name}`, err)
      commentsTableView.push(raw('all the judges', v.team.name, 'NO COMMENTS'))
    }
  }

  // JUDGES' GRADES
  const judges = await judgeEsb.getList({ eventId }, orgId);
  const judgeGradesTableView = [];
  for (const j of judges) {
    judgeGradesTableView.push(
      raw('Судья', j.name),
      raw('Команда', 'Критерий 1', 'Критерий 2', 'Критерий 3', 'Критерий 4', 'Критерий 5'), // TODO: use var for num of criterias
    );

    try {
      const queryRes = await verdictsDb.getByJudge(j.id);
      if (queryRes.rows.length > 0) {
        judgeGradesTableView.push(queryRes.rows.map(g => raw(g.team, g.criteria_1, g.criteria_2, g.criteria_3, g.criteria_4, g.criteria_5)))
      } else {
        judgeGradesTableView.push(raw('NO GRADES'))
      }
    } catch (err) {
      console.log(`Fetching judge ${j.id} grades err`, err);
    }

    judgeGradesTableView.push(raw())
  }

  return {
    verdicts: verdictsTableView,
    comments: commentsTableView,
    judgeGrades: judgeGradesTableView,
  };
}

module.exports = { getData };
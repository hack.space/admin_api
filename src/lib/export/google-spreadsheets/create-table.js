const { getData } = require('./parser');
const { google } = require('googleapis');

async function exportToSpreadshhets(auth, eventId, orgId) {
  let data = await getData(eventId, orgId);

  const sheets = google.sheets({ version: 'v4', auth });

  var request = {
    resource: {
      properties: {
        title: 'Результаты голосования ' + new Date()
      },
      sheets: [{
        data: [{
          rowData: data.verdicts
        }]
      }, {
        data: [{
          rowData: data.comments
        }]
      },
      {
        data: [{
          rowData: data.judgeGrades
        }]
      }]
    },

    auth,
  };


  return new Promise((res, rej) => {
    sheets.spreadsheets.create(request, function (err, response) {
      if (err) {
        return rej(err);
      }
      return res(response.data.spreadsheetUrl);
    });
  });
}

module.exports = {
  exportToSpreadshhets,
};
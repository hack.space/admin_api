// const id = process.env.SHEETS_ID;
// const secret = process.env.SHEETS_SECRET;

const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const { getData } = require('./parser');
const [, , eventId, orgId] = process.argv;

const SCOPES = [
  'https://www.googleapis.com/auth/spreadsheets'
];
const TOKEN_PATH = 'token.json';

// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Google Sheets API.
  authorize(JSON.parse(content), createTable);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const { client_secret, client_id, redirect_uris } = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
    client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  rl.question('Enter the code from that page here: ', (code) => {
    rl.close();
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error while trying to retrieve access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
  });
}

async function createTable(auth) {
  let data;
  try {
    data = await getData(eventId, orgId);
  } catch (err) {
    console.log('cannot fetch data', err);
    process.exit(-1);
  }

  const sheets = google.sheets({ version: 'v4', auth });

  var request = {
    resource: {
      properties: {
        title: 'Результаты голосования ' + new Date()
      },
      sheets: [{
        data: [{
          rowData: data.verdicts
        }]
      }, {
        data: [{
          rowData: data.comments
        }]
      },
      {
        data: [{
          rowData: data.judgeGrades
        }]
      }]
    },

    auth,
  };

  sheets.spreadsheets.create(request, function (err, response) {
    if (err) {
      console.error(err);
      return;
    }
    console.log(response);
    // TODO: Change code below to process the `response` object:
    // console.log(JSON.stringify(response, null, 2));
  });
}
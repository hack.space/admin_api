const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const actorDb = require('../controllers/actors.controller');

let unit = {};

const SECRET = process.env.SECRET;

if (!SECRET) {
  console.error('Environment variable SECRET is required.');
  process.exit(-1);
}

/**
 * Generates jwt token.
 */
unit.makeToken = (id, orgId, meta, expiresIn = '60d') => {
  return jwt.sign({ id, orgId, ...meta }, SECRET, { expiresIn });
}

let decodeToken = (token) => {
  return jwt.verify(token, SECRET);
}

/**
 * This module represents a middleware for checking
 * user's authentication.
 */
async function authenticate(req, res, next) {
  try {
    let token = req.headers['authorization'];
    let data = decodeToken(token);

    if (data.role !== 'judge') {
      let getQueryRes = await actorDb.getById(data.id, data.orgId);
      if (getQueryRes.rows.length === 0)
        throw {}
      else if (getQueryRes.rows.length > 1)
        return res.status(500).send({ message: 'count is out of range' });
      data.role = getQueryRes.rows[0].accesslevel;
    }

    req._acl = data;
    next();
  } catch (err) {
    res.status(401).send();
  }
}

unit.middleware = () => {
  return authenticate;
}

unit.decodeToken = decodeToken;

module.exports = unit;
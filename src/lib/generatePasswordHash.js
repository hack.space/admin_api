const crypto = require('crypto')

/**
 * Generates salt and pwdHash from it.
 * @param {string} pwd user's password
 * @param {string?} mySalt if not set, will be generated
 * @returns {object} containing generated `salt` and `pwdHash`
 */
let generatePasswordHash = (pwd, mySalt = '') => {
  let salt = mySalt || crypto.randomBytes(128)
    .toString('hex')
    .slice(0, 256);

  let pwdHash = crypto.createHmac('sha256', salt)
    .update(pwd)
    .digest('hex');

  return { salt, pwdHash };
}

module.exports = generatePasswordHash;
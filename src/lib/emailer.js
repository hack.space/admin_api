const nodemailer = require('nodemailer');
const fs = require('fs');
const { join } = require('path');
const config = require('../config');

const EMAIL_USER = process.env.EMAIL_USER;
const EMAIL_PWD = process.env.EMAIL_PWD;
const JUDGE_LINK_PREFIX = config.url + `/voting/welcome/`
let htmlTemplate = fs.readFileSync(join(__dirname, 'templates', 'email.html'), 'utf8');

let smtpTransport;
try {
  smtpTransport = nodemailer.createTransport({
    host: 'smtp.yandex.ru',
    port: 465,
    secure: true, // true for 465, false for other ports 587
    auth: {
      user: EMAIL_USER,
      pass: EMAIL_PWD
    }
  });
} catch (err) {
  console.log('Init mailer error:', err)
  process.exit(-1);
}

let unit = {}

/**
 * Sends invitation letter
 * @param {string[]} receivers list of receivers
 * @param {object} details - for the letter's customization
 * @param {string} details.name - name of a judge
 * @param {string} details.link - redirect link to judging
 * @param {string} details.eventTitle - name of an event
 */
unit.sendMail = (receivers, details) => {
  receivers = typeof receivers === 'string' ? receivers : receivers.join(',');
  let mailOptions = {
    from: EMAIL_USER,
    to: receivers, // list of receivers comma separated
    subject: 'Invitation for judging at ' + details.eventTitle,
    html: customizeLetter(details) // html body
  };

  smtpTransport.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.error('Error', err);
      return
    }

    console.debug('Message sent: %s', JSON.stringify(info));
  })
}

function customizeLetter({ name, link, eventTitle }) {
  return htmlTemplate
    .replace('#judge_name', name)
    .replace('#redirect_link', link)
    .replace('#event_title', eventTitle);
}

module.exports = unit;
const { getStatusText } = require('http-status-codes');

let errorHandler = function (err, res) {
  let code;
  if (err.code >= 400 && err.code < 500)
    code = err.code;
  else code = 500;

  let message = err.message ? err.message : getStatusText(code);

  if (code === 500)
    console.error(err);

  return res.status(code).send({ message });
}

module.exports = errorHandler;
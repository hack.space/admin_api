const participantsDb = require('../controllers/participants.controller')
const getListSchema = require('../../apidoc/schemas/participants/get.list.schema.json')
const { validate } = require('../lib/validator')
const eventDb = require('../controllers/events.controller')

let unit = {};

let outputMappper = r => ({
  id: r.id,
  username: r.username,
  pic: r.pic,
  contactPhone: r.contactphone,
  email: r.email,
  status: r.status,
  statusOnHold: r.status_on_hold || null,
  skills: r.skills,
  tgProfileLink: r.tgprofilelink,
  isSearchable: r.bool_or,
  xp: +r.xp,

  bio: r.bio,
  addr: r.addr,
  contact: r.slackdeeplink || r.email || r.tgProfileLink,
  slackId: r.slackid,

  team: r.teamName,
});

async function checkAccess(eventId, orgId, errCode = 404) {
  let getQueryRes = await eventDb.getById(eventId, orgId)
  let event = getQueryRes.rows[0];
  if (!event)
    return Promise.reject({ code: errCode });
  return event;
}

unit.getList = async (eventId, orgId) => {
  try {
    await checkAccess(eventId, orgId, 403);

    let { rows } = await participantsDb.getList(eventId);
    let outputList = rows.map(outputMappper);
    return outputList;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message })
  }
}

/**
 * @param newStatus - one of ['confirmed', 'declined']
 */
unit.updateParticipationStatus = async (eventId, hackerId, orgId, newStatus) => {
  await checkAccess(eventId, orgId, 404);
  if (['confirmed', 'declined'].indexOf(newStatus) < 0) {
    throw { code: 400, message: 'newStatus should be one of ["confirmed", "declined"]' }
  }
  eventId = Number(eventId);
  hackerId = Number(hackerId);
  // TODO: user new BadResponseError(code, msg) syntax
  if (!eventId) {
    throw { code: 400, message: 'eventId should be a number' }
  }
  if (!hackerId) {
    throw { code: 400, message: 'hackerId should be a number' }
  }

  const curPrtStatus = await participantsDb.getParticipationStatus(eventId, hackerId);
  if (!curPrtStatus) {
    throw { code: 404 };
  }
  if (curPrtStatus !== 'applied') {
    throw { code: 400, message: 'Current participation status should be "applied"' };
  }

  await participantsDb.updateStatusOnHold(eventId, hackerId, newStatus);
  return { result: true };
}

unit.releaseOnHold = async (eventId, orgId) => {
  await checkAccess(eventId, orgId, 404);
  await participantsDb.setStatusesFromOnHold(eventId);
  return { result: true };
}
module.exports = unit;
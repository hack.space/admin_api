const { validate } = require('jsonschema');
const createSchema = require('../../apidoc/schemas/actors/create.schema.json');
const updateSchema = require('../../apidoc/schemas/actors/update.schema.json');
const db = require('../controllers/actors.controller');

let unit = {};

let outputMapper = dbObj => ({
  id: dbObj.id,
  username: dbObj.username,
  role: dbObj.accesslevel,
  email: dbObj.email
})

/**
 * Creates new actor in DB. 
 * @param {object} obj actor's object
 * @param {string} orgId id of actor's organization
 */
unit.create = async (obj, orgId) => {
  // TODO: send comfirmation mail to new actor
  try {
    if (parseInt(orgId) != orgId)
      throw { code: 500, message: "Incorrect orgId in _acl: " + orgId }

    let validationRes = validate(obj, createSchema);
    if (!validationRes.valid)
      throw {
        code: 400,
        message: validationRes.errors
      };

    let createQueryRes = await db.create(obj, orgId);
    let { id } = createQueryRes.rows[0];

    return unit.getById(id, orgId);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

/**
 * Updates actors row in database.
 * @param {string} id of actor to update
 * @param {object} obj new actor object
 * @param {string} orgId id of organization
 * @returns {promise} resolves new actor
 */
unit.update = async (id, obj, orgId) => {
  try {
    let validationRes = validate(obj, updateSchema)
    if (!validationRes.valid)
      throw {
        code: 400,
        message: validationRes.errors
      };

    let exActor = await unit.getById(id, orgId); // reject with 404 if no that user
    if (!obj.pwd
      && obj.username === exActor.username
      && obj.role === exActor.role
      && obj.email === exActor.email)
      return exActor;

    if (exActor.role === 'admin' && obj.role === 'basic')
      await checkLastAdmin(orgId);

    // TODO: if update email -> send  confirmation mail using incoming mail service

    await db.update(id, obj, orgId)
    return unit.getById(id, orgId)
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.delete = async (id, orgId) => {
  try {
    let exActor = await unit.getById(id, orgId);
    if (!exActor)
      throw { code: 404 }
    if (exActor.role === 'admin')
      await checkLastAdmin(orgId);

    await db.delete(id, orgId);
    return Promise.resolve();
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getList = async (orgId) => {
  try {
    let getQueryRes = await db.getList(orgId);
    let actors = getQueryRes.rows.map(outputMapper);
    return actors;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getById = async (id, orgId) => {
  try {
    if (parseInt(id) != id)
      throw { code: 404 }
    let getQueryRes = await db.getById(id, orgId);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }
    else if (getQueryRes.rows.length > 1)
      throw { code: 500 }
    let actor = getQueryRes.rows.map(outputMapper)[0];
    return actor;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

/**
 * Check that it is not last admin in organization.
 * @param {string} orgId id of organization
 * @throws 400 if it is
 * @returns {boolean} true if ok (not a last admin)
 */
let checkLastAdmin = async (orgId) => {
  let actors = await unit.getList(orgId);
  let adminCount = actors
    .map(a => a.role === 'admin')
    .reduce((acc, cur) => acc + cur);
  if (adminCount == 1) // not strict, because adminCount can contain 'true' value
    throw { code: 400, message: 'cannot delete last admin' };
  return true;
}

module.exports = unit;
const { validate } = require('jsonschema');
const createSchema = require('../../apidoc/schemas/teams/create.schema.json');
const updateSchema = require('../../apidoc/schemas/teams/update.schema.json');
const db = require('../controllers/teams.controller');
const eventDb = require('../controllers/events.controller');

let unit = {};

let outputMapper = dbObj => ({
  id: dbObj.id,
  name: dbObj.name,
  order: dbObj.speech_order,
  eventId: dbObj.event_id,
  hideList: dbObj.hide_list,
  description: dbObj.twit,
  limit: dbObj.max_members,
  leadId: dbObj.teamlead_id,
  find: dbObj.is_searchable,
  idea: dbObj.idea,
  repo: dbObj.repo,
  slides: dbObj.slides,
  members: dbObj.members,
})

async function checkAccess(eventId, orgId, errCode = 404) {
  let getQueryRes = await eventDb.getById(eventId, orgId)
  let event = getQueryRes.rows[0];
  if (!event)
    return Promise.reject({ code: errCode });
  return event;
}

const hideTeams = (teams, hideFromId) => teams.filter(t => t.hideList.indexOf(hideFromId) === -1);

/**
 * Creates new team in DB. 
 * @param {object} obj team's object
 * @param {string} orgId id of team's organization
 */
unit.create = async (obj, orgId) => {
  try {
    validate(obj, createSchema);
    let { eventId } = obj;
    await checkAccess(eventId, orgId, 403);

    // set default order if empty -> put in the end
    obj.order = obj.order || 1000;

    let createQueryRes = await db.insert(obj);
    let { id } = createQueryRes.rows[0];

    return unit.getById(id, orgId);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

/**
 * Updates teams row in database.
 * @param {string} id of team to update
 * @param {object} obj new team object
 * @param {string} orgId id of organization
 * @returns {promise} resolves new team
 */
unit.update = async (id, obj, orgId) => {
  try {
    if (id != obj.id)
      throw {code: 400, message: "id in parameter doesn't equal id in body"}
    validate(obj, updateSchema)
    let { eventId } = obj;
    await checkAccess(eventId, orgId, 403);

    // set default order
    obj.order = obj.order || 1000;

    let exTeam = await unit.getById(id, orgId); // reject with 404 if no that user
    let theSame = true;
    for (let key in obj)
      theSame &= obj[key] === exTeam[key]
    if (theSame)
      return exTeam;

    await db.update(obj)
    return unit.getById(id, orgId)
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.delete = async (id, orgId) => {
  try {
    let queryRes = await db.get(id);
    if (queryRes.rows.length === 0)
      throw { code: 404 }
    await checkAccess(queryRes.rows[0].event_id, orgId, 404);
    await db.delete(id);
    return Promise.resolve();
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getList = async (query, orgId, hideFromJudgeId) => {
  try {
    let { eventId } = query;
    if (!eventId || parseInt(eventId) != eventId)
      throw { code: 400, message: 'query parameter eventId is required (number)' }
    await checkAccess(eventId, orgId, 403);
    let getQueryRes = await db.getList(eventId);
    let teams = getQueryRes.rows.map(outputMapper);
    return hideFromJudgeId ? hideTeams(teams, hideFromJudgeId) : teams;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getById = async (id, orgId) => {
  try {
    if (parseInt(id) != id)
      throw { code: 404 }
    let getQueryRes = await db.get(id);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }
    let team = getQueryRes.rows.map(outputMapper)[0];
    await checkAccess(team.eventId, orgId, 404)
    return team;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
const { validate } = require('../lib/validator');
const createSchema = require('../../apidoc/schemas/events/create.schema.json');
const updateSchema = require('../../apidoc/schemas/events/update.schema.json');
const db = require('../controllers/events.controller');
const judgeESB = require('../ESBs/judges.esb')
const teamESB = require('../ESBs/teams.esb')
const criteriaESB = require('../ESBs/criterias.esb')
const generateHash = require('../lib/generatePasswordHash');

let unit = {};

let outputMapper = (o) => {
  let rg = /lat\:([\d\-\.]+),lng\:([\d\-\.]+)/i

  let [, lat, lng] = o.location ? rg.exec(o.location) : []; // event's location

  return {
    id: o.id,
    title: o.title,
    city: o.city,
    dateStart: o.datestart,
    dateEnd: o.dateend,
    description: o.description,
    link: o.link,
    preview: o.preview,
    eventType: o.eventtype,
    schedule: o.schedule,
    enteringWord: o.enteringword,
    checkInLocation: { lat, lng },
    autoconfirmation: o.autoconfirmation,
    isDraft: o.isdraft,
    participants: o.participants || 0,
    judgingEnabled: o.judging_enabled,
    colors: o.colors,
    p2pEnabled: o.p2p_enabled,
    p2pMaxCount: +o.p2p_max_count,
    orgId: o.organizationid,
    logo: o.judge_logo,
    helloMessage: o.hello_message,
    starIconType: o.judge_star_icon_type,
    commentsEnabled: o.judge_comments_enabled === "true" || o.judge_comments_enabled === true,
  }
}

/**
 * Creates new event in DB. 
 * @param {object} obj event's object
 * @param {string} orgId id of event's organization
 */
unit.create = async (obj, orgId) => {
  try {
    await validate(obj, createSchema);

    // TBR: when will be fixed on front-end side
    delete obj.participants

    let { id } = (await db.create(obj, orgId)).rows[0];
    return unit.getById(id, orgId);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

/**
 * Updates events row in database.
 * @param {object} obj new event object
 * @param {string} eventId of event to update
 * @param {string} orgId id of organization
 * @returns {promise} resolves new event
 */
unit.update = async (obj, eventId, orgId) => {
  try {
    await validate(obj, updateSchema);
    if (eventId != obj.id)
      throw { code: 400, message: 'id in object does not equal id in parameter' };
    let cur = await unit.getById(eventId, orgId);
    if (!cur.isDraft)
      throw { code: 403 }

    await db.update(obj, eventId, orgId)
    return unit.getById(eventId, orgId);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.delete = async (eventId, orgId) => {
  try {
    let cur = await unit.getById(eventId, orgId);
    if (!cur.isDraft)
      throw { code: 403 }
    await db.delete(eventId, orgId)
    return;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getList = async (orgId) => {
  // TODO: add filtering, paging, subgroups
  try {
    let getQueryRes = await db.getList(orgId);
    return getQueryRes.rows.map(outputMapper);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getById = async (eventId, orgId) => {
  try {
    let getQueryRes = await db.getById(eventId, orgId);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }
    return outputMapper(getQueryRes.rows[0])
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.startJudging = async (eventId, orgId) => {
  try {
    const event = await unit.getById(eventId, orgId);
    if (event.judgingEnabled) {
      await db.updateJudgingEnabled(eventId, orgId, false);
      return;
    }
    let judges = await judgeESB.getList({ eventId }, orgId);
    if (judges.length < 1)
      throw { code: 400, message: 'At least one judge is required' }
    let criterias = await criteriaESB.getList({ eventId }, orgId);
    if (criterias.length < 1)
      throw { code: 400, message: 'At least one criteria is required' }
    let teams = await teamESB.getList({ eventId }, orgId);
    if (teams.length < 2)
      throw { code: 400, message: 'At least two teams are required' }
    await db.updateJudgingEnabled(eventId, orgId, true);
    return;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getByHash = async (hash) => {
  try {
    const getQueryRes = await db.getByHash(hash);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }
    const event = outputMapper(getQueryRes.rows[0]);
    // if (hash !== generateHash(event.enteringWord, event.id + '').pwdHash)
    //   throw { code: 404 }
    return event;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
const { validate } = require('jsonschema');
const createSchema = require('../../apidoc/schemas/criterias/create.schema.json');
const updateSchema = require('../../apidoc/schemas/criterias/update.schema.json');
const db = require('../controllers/criterias.controller');
const eventDb = require('../controllers/events.controller');

let unit = {};

let outputMapper = dbObj => ({
  id: dbObj.id,
  name: dbObj.name,
  description: dbObj.description,
  scaleFrom: dbObj.scale_from,
  scaleTo: dbObj.scale_to,
  eventId: dbObj.event_id,
  weight: +dbObj.weight,
  polyTitle: dbObj.poly_title,
})

async function checkAccess(eventId, orgId, errCode = 404) {
  let getQueryRes = await eventDb.getById(eventId, orgId)
  let event = getQueryRes.rows[0];
  if (!event)
    return Promise.reject({ code: errCode });
}

/**
 * Creates new criteria in DB. 
 * @param {object} obj criteria's object
 * @param {string} orgId id of criteria's organization
 */
unit.create = async (obj, orgId) => {
  try {
    validate(obj, createSchema);
    let { eventId } = obj;
    await checkAccess(eventId, orgId, 403);

    // set default scale if empty
    obj.scaleFrom = Number.isInteger(obj.scaleFrom) ? obj.scaleFrom : 1;
    obj.scaleTo = Number.isInteger(obj.scaleTo) ? obj.scaleTo : 5;
    obj.weight = obj.weight && obj.weight > 0 ? obj.weight : 1;

    let createQueryRes = await db.insert(obj);
    let { id } = createQueryRes.rows[0];

    return unit.getById(id, orgId);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

/**
 * Updates criterias row in database.
 * @param {string} id of criteria to update
 * @param {object} obj new criteria object
 * @param {string} orgId id of organization
 * @returns {promise} resolves new criteria
 */
unit.update = async (id, obj, orgId) => {
  try {
    if (id != obj.id)
      throw {code: 400, message: "id in parameter doesn't equal id in body"}
    validate(obj, updateSchema)
    let { eventId } = obj;
    await checkAccess(eventId, orgId, 403);

    // set default scale if empty
    obj.scaleFrom = obj.scaleFrom || 1;
    obj.scaleTo = obj.scaleTo || 5;
    obj.weight = obj.weight && obj.weight > 0 ? obj.weight : 1;

    let exCriteria = await unit.getById(id, orgId); // reject with 404 if no that user
    let theSame = true;
    for (let key in obj)
      theSame &= obj[key] === exCriteria[key]
    if (theSame)
      return exCriteria;

    await db.update(obj)
    return unit.getById(id, orgId)
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.delete = async (id, orgId) => {
  try {
    let queryRes = await db.get(id);
    if (queryRes.rows.length === 0)
      throw { code: 404 }
    await checkAccess(queryRes.rows[0].event_id, orgId, 404);
    await db.delete(id);
    return Promise.resolve();
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getList = async (query, orgId) => {
  try {
    let { eventId } = query;
    if (!eventId || parseInt(eventId) != eventId)
      throw { code: 400, message: 'query parameter eventId is required (number)' }
    await checkAccess(eventId, orgId, 403);
    let getQueryRes = await db.getList(eventId);
    let criterias = getQueryRes.rows.map(outputMapper);
    return criterias;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getById = async (id, orgId) => {
  try {
    if (parseInt(id) != id)
      throw { code: 404 }
    let getQueryRes = await db.get(id);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }
    let criteria = getQueryRes.rows.map(outputMapper)[0];
    await checkAccess(criteria.eventId, orgId, 404)
    return criteria;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
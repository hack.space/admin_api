const { validate } = require('jsonschema');
const updateSchema = require('../../apidoc/schemas/verdicts/update.schema.json');
const updateListSchema = require('../../apidoc/schemas/verdicts/update.list.schema.json');
const db = require('../controllers/verdicts.controller');
const eventDb = require('../controllers/events.controller');
const eventEsb = require('../ESBs/events.esb');
const criteriaEsb = require('../ESBs/criterias.esb');
const teamEsb = require('../ESBs/teams.esb');
const commentDb = require('../controllers/comments.controller');

let unit = {};

let outputMapper = (dbObj, p2pCoef = 0) => {
  const jury = +dbObj.average_weighted || 0;
  const peers = +dbObj.average_weighted_peers || 0;
  const averageWeighted = +(jury * (1 - p2pCoef) + peers * p2pCoef).toFixed(4);

  return {
    team: {
      id: dbObj.id,
      name: dbObj.name,
      order: dbObj.speech_order
    },
    criterias: dbObj.criterias,
    summary: {
      sum: +dbObj.sum,
      averageWeighted,
      averagePeers: peers,
      averageJury: jury,
    }
  }
};

let outputSingleMapper = dbObj => ({
  team: {
    id: dbObj.id,
    name: dbObj.name,
    order: dbObj.speech_order
  },
  criterias: dbObj.criterias
})

const outputCommentsMapper = dbObj => ({
  teamId: dbObj.team_id,
  judgeId: dbObj.judge_id,
  judgeName: dbObj.judge_name,
  comment: dbObj.comment,
});

const getCriteriaName = (criteria, team) => {
  console.debug('get-criteria-name', criteria.polyTitle, team.order);
  if (!criteria.polyTitle)
    return criteria.name;

  // criteria.name == option1 | option2 | ... | optionN
  // we define choosen option based on team group
  // team group is defined by its order, 0015 - first group, 2015 - third group

  const options = criteria.name.split('|');
  let teamGroup = Math.floor(team.order / 1000);
  if (teamGroup > options.length - 1)
    teamGroup = options.length - 1;
  console.debug('poly-title', options, teamGroup);
  return options[teamGroup].trim();
}

async function checkAccess(eventId, orgId, errCode = 404) {
  let getQueryRes = await eventDb.getById(eventId, orgId)
  let event = getQueryRes.rows[0];
  if (!event)
    return Promise.reject({ code: errCode });
  return event;
}

async function checkAccessByTeam(teamId, eventId, orgId, errCode = 404) {
  let team = await teamEsb.getById(teamId, orgId)
  if (!team || team.eventId != eventId)
    return Promise.reject({ code: errCode });
  return team;
}

async function checkAccessByCriteria(criteriaId, eventId, orgId, errCode = 404) {
  let criteria = await criteriaEsb.getById(criteriaId, orgId)
  if (!criteria || criteria.eventId != eventId)
    return Promise.reject({ code: errCode });
  return criteria;
}

unit.update = async (verdictDTO, teamId, judgeId, eventId, orgId) => {
  try {
    const event = await eventEsb.getById(eventId, orgId);
    if (!event.judgingEnabled)
      throw { code: 400, message: 'Judging dissabled' }
    if (verdictDTO.teamId != teamId)
      throw { code: 400, message: "id in parameter doesn't equal id in body" }
    validate(verdictDTO, updateSchema)

    await checkAccessByTeam(teamId, eventId, orgId, 403);

    for (let c of verdictDTO.criterias) {
      await checkAccessByCriteria(c.id, eventId, orgId, 403);
      // TODO: c.points < scaleFrom || c.points > scaleTo
      if (c.points < 0 || c.points > 10) {
        throw { code: 400, message: 'points should be in [0,10] interval' }
      }
    }

    for (let c of verdictDTO.criterias) {
      await db.update(judgeId, teamId, c.id, c.points);
    }

    if (event.commentsEnabled && verdictDTO.comment) {
      await commentDb.update(judgeId, teamId, verdictDTO.comment);
    }

    return unit.getById(teamId, judgeId, eventId, orgId)
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.updateList = (verdictsDTO, judgeId, eventId, orgId) => {
  validate(verdictsDTO, updateListSchema);
  return Promise.all(
    verdictsDTO.map(verdict => unit.update(verdict, verdict.teamId, judgeId, eventId, orgId))
  );
}

unit.getList = async (query, orgId) => {
  try {
    let { eventId } = query;
    if (!eventId || parseInt(eventId) != eventId)
      throw { code: 400, message: 'query parameter eventId is required (number)' }
    const event = await checkAccess(eventId, orgId, 403);
    let getQueryRes = await db.getList(eventId, event.p2p_enabled);
    let verdicts = getQueryRes.rows.map(row => outputMapper(row, event.p2p_coef || 0));
    let criterias = await criteriaEsb.getList({ eventId }, orgId);
    verdicts.forEach(v => {
      criterias.map(c => {
        let found = v.criterias.find(vc => vc.id === c.id);
        if (!found)
          v.criterias.push({
            id: c.id,
            name: c.name,
            scaleFrom: c.scaleFrom,
            scaleTo: c.scaleTo,
            sum: 0,
            average: 0,
          })
      })
    })
    return verdicts;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getById = async (teamId, judgeId, eventId, orgId) => {
  try {
    if (parseInt(teamId) != teamId)
      throw { code: 404 }
    if (!judgeId || !eventId)
      throw { code: 401 }

    let criterias = await criteriaEsb.getList({ eventId }, orgId);
    let team = await teamEsb.getById(teamId, orgId);
    console.debug(JSON.stringify(criterias));
    console.debug(JSON.stringify(team));
    let getQueryRes = await db.get(teamId, judgeId, eventId);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }

    let verdict = outputSingleMapper(getQueryRes.rows[0]);

    criterias.map(c => {
      let found = verdict.criterias.find(vc => vc.id === c.id);
      if (!found)
        verdict.criterias.push({
          id: c.id,
          name: getCriteriaName(c, team),
          scaleFrom: c.scaleFrom,
          scaleTo: c.scaleTo,
          description: c.description,
          points: 0
        })
      else
        found.name = getCriteriaName(c, team)
    })

    const commentRes = await commentDb.get(judgeId, teamId); 
    verdict.comment = commentRes.rows[0] && commentRes.rows[0].comment;

    return verdict;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getCommentsByTeam = async (teamId) => {
  try {
    if (parseInt(teamId) != teamId)
      throw { code: 404 }

    const getCommentsRes = await commentDb.getList(teamId);
    if (getCommentsRes.rows.length === 0)
      throw { code: 404 }

    const comments = getCommentsRes.rows.map(outputCommentsMapper);
    
    return comments;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
const eventEsb = require('../ESBs/events.esb')
const judgeEsb = require('../ESBs/judges.esb')
const judgeDb = require('../controllers/judges.controller')
const criteriasEsb = require('../ESBs/criterias.esb')

let unit = {}

unit.start = async (id, eventId, orgId) => {
  try {
    let event = await eventEsb.getById(eventId, orgId);
    let judge = await judgeEsb.getById(id, orgId);
    await judgeDb.updateStatus(judge.id, 'activated');
    return {
      judgeId: judge.id,
      judgeName: judge.name,
      eventId: event.id,
      eventName: event.title,
      started: event.judgingEnabled,
      colors: event.colors || null,
      logo: event.logo,
      helloMessage: event.helloMessage,
      starIconType: event.starIconType,
      commentsEnabled: event.commentsEnabled,
      criterias: await criteriasEsb.getList({eventId}, orgId)
    }
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.pregenerateAndStart = async (hash) => {
  try {
    const event = await eventEsb.getByHash(hash); // throws 404 if no such event
    const { judgingEnabled, p2pEnabled, p2pMaxCount: max } = event;

    if (!judgingEnabled || !p2pEnabled)
      throw { code: 404 }

    if (max !== 0) {
      const guestCount = await judgeEsb.count(event.id, 1);
      console.log(`${event.title}: already registered ${guestCount} guests`);
      if (guestCount >= max)
        throw { code: 404 }
    } 
    const guestJudge = {
      name: 'дорогой гость', // TODO: translate
      email: 'guest@hack.space',
      eventId: event.id,
      type: 1, // TODO: add guest (2) type
    }
    const newJudge = await judgeEsb.create(guestJudge, event.orgId)
    return newJudge.judgingLink;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
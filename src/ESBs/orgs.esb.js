const db = require('../controllers/orgs.controller');
const googleAuth = require('../lib/export/google-spreadsheets/google-auth');

let unit = {};

let outputMapper = dbObj => ({
  id: dbObj.id,
  title: dbObj.title,
  website: dbObj.website,
  logo: dbObj.logo
});

unit.getById = async (id) => {
  try {
    if (parseInt(id) != id)
      throw { code: 404 };
    let org = await db.getById(id);
    if (org.length)
      throw {code: 404}

    return outputMapper(org.rows[0]);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.setToken = async (id, code) => {
  try {
    const tokens = await googleAuth.getNewToken(code);
    await db.setGoogleToken(id, JSON.stringify(tokens));
    return {result: 'ok'};
  } catch (err) {
    console.log(err);
    return {result: 'failed'};
  }
}

unit.getToken = async (id) => {
  const {google_token: token} = await db.getGoogleToken(id);
  try {
    return JSON.parse(token);
  } catch (err) {
    return null
  }
}

module.exports = unit;
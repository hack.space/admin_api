const db = require('../controllers/actors.controller');
const { makeToken } = require('../lib/authN');
const generatePasswordHash = require('../lib/generatePasswordHash');

let unit = {};

/**
 * Log user in to the platform.
 * @param {string} login - actor's username
 * @param {string} pwd - actor's password
 * @returns {promise} resolves token
 */
unit.login = async (login, pwd) => {
  try {
    if (typeof login !== 'string' || typeof pwd !== 'string')
      throw { code: 400 }
    login = login.toLowerCase();
    let getQueryRes = await db.getByEmail(login);
    if (getQueryRes.rows.length === 0)
      throw { code: 401 };
    else if (getQueryRes.rows.length > 1)
      throw { code: 500 };

    let actor = getQueryRes.rows[0];
    let { pwdHash } = generatePasswordHash(pwd, actor.salt);
    if (pwdHash !== actor.pwd)
      throw { code: 401 };

    let result = {
      token: makeToken(actor.id, actor.organizationid, { role: actor.accessLevel })
    }
    return Promise.resolve(result);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
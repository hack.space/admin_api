const msgDb = require('../controllers/messages.controller');
const eventDb = require('../controllers/events.controller');
const { validate } = require('../lib/validator');
const createSchema = require('../../apidoc/schemas/messages/create.schema.json')
const request = require('request');

const BOT_URL = process.env.BOT_URL;
const BOT_TOKEN = process.env.TG_BOT_TOKEN;

if (!BOT_URL) {
  console.error('BOT_URL environment variable is required');
  process.exit(-1);
}

if (!BOT_TOKEN) {
  console.error('TG_BOT_TOKEN environment variable is required');
  process.exit(-1);
}

let unit = {};

async function checkAccess(eventId, orgId, errCode = 404) {
  let getQueryRes = await eventDb.getById(eventId, orgId);
  let event = getQueryRes.rows[0];
  if (!event)
    return Promise.reject({ code: errCode });
}

unit.getList = async (params, orgId) => {
  try {
    let lastMessageTimestamp = params.lastMessageTimestamp || 0;
    let eventId = params.eventId;

    if (!eventId)
      throw { code: 400, message: 'eventId is missing' };
    await checkAccess(eventId, orgId);

    let getQueryRes = await msgDb.getMessagesList(eventId, lastMessageTimestamp, orgId);
    let output = {
      messages: getQueryRes.rows.map(m => ({
        id: m.id,
        eventId: m.eventid,
        postTimestamp: m.posttimestamp,
        content: m.content
      }))
    };
    return output;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message })
  }
};

unit.post = async (obj, orgId) => {
  try {
    await validate(obj, createSchema);
    let eventId = obj.eventId;
    let text = obj.message;
    await checkAccess(eventId, orgId, 403);
    await msgDb.createMessage(eventId, text);

    let receiversQueryRes = await msgDb.getReceivers(eventId);
    let receivers = receiversQueryRes.rows.map(r => r.tgid);
    let messages = [];
    for (let user_tg_id of receivers)
      if (user_tg_id)
        messages.push({ user_tg_id, text });

    request({
      method: 'post',
      url: `${BOT_URL}/${BOT_TOKEN}/send_messages`,
      json: true,
      body: messages
    });

    return;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message })
  }
};

module.exports = unit;
const orgsService = require('../ESBs/orgs.esb');
const {exportToSpreadshhets} = require('../lib/export//google-spreadsheets/create-table');
const googleAuth = require('../lib/export/google-spreadsheets/google-auth');

const exportResults = async (eventId, actorId, orgId) => {
  const token = await orgsService.getToken(actorId);
  if (!token) {
    const url = await googleAuth.getLoginUrl();
    return {result: 'need_token', link: url};
  }
  const auth = await googleAuth.authorize(token);
  const sheetUri = await exportToSpreadshhets(auth, eventId, orgId);
  return {result: 'ok', link: sheetUri};
}

module.exports = {
  exportResults,
}
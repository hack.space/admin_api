const { validate } = require('../lib/validator');
const createSchema = require('../../apidoc/schemas/judges/create.schema.json');
const updateSchema = require('../../apidoc/schemas/judges/update.schema.json');
const inviteSchema = require('../../apidoc/schemas/judges/invite.schema.json')
const db = require('../controllers/judges.controller');
const eventDb = require('../controllers/events.controller');
const { makeToken } = require('../lib/authN')
const emailer = require('../lib/emailer');

// depending on PROD or DEV environment
const { url, JUDGE_BASE_URL } = require('../config');

let unit = {};

let outputMapper = dbObj => ({
  id: dbObj.id,
  name: dbObj.name,
  email: dbObj.email,
  judgingLink: `${JUDGE_BASE_URL || url}/voting/${dbObj.event_id == 19 ? 'guests/' : ''}welcome/${dbObj.token}`,
  eventId: dbObj.event_id,
  status: dbObj.status
})

async function checkAccess(eventId, orgId, errCode = 404) {
  let getQueryRes = await eventDb.getById(eventId, orgId)
  let event = getQueryRes.rows[0];
  if (!event)
    return Promise.reject({ code: errCode });
  return event;
}

/**
 * Creates new judge in DB. 
 * @param {object} obj judge's object
 * @param {string} orgId id of judge's organization
 */
unit.create = async (obj, orgId) => {
  try {
    await validate(obj, createSchema);
    let { eventId } = obj;
    await checkAccess(eventId, orgId, 403);

    // create then get it, after that we can make a token with it
    let createQueryRes = await db.insert(obj);
    obj.id = createQueryRes.rows[0].id;

    obj.token = makeToken(obj.id, orgId, {
      eventId,
      role: 'judge',
      type: obj.type || 0
    })

    // update with new token 
    await db.update(obj)
    return unit.getById(obj.id, orgId)
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

/**
 * Updates judges row in database.
 * @param {string} id of judge to update
 * @param {object} obj new judge object
 * @param {string} orgId id of organization
 * @returns {promise} resolves new judge
 */
unit.update = async (id, obj, orgId) => {
  try {
    if (id != obj.id)
      throw { code: 400, message: "id in parameter doesn't equal id in body" }
    await validate(obj, updateSchema)
    let { eventId } = obj;
    await checkAccess(eventId, orgId, 403);

    delete obj.status
    delete obj.judgingLink

    let exJudge = await unit.getById(id, orgId); // reject with 404 if no that user
    let theSame = true;
    for (let key in obj)
      theSame &= obj[key] === exJudge[key]
    if (theSame)
      return exJudge;

    await db.update(obj)
    return unit.getById(id, orgId)
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.delete = async (id, orgId) => {
  try {
    let queryRes = await db.get(id);
    if (queryRes.rows.length === 0)
      throw { code: 404 }
    await checkAccess(queryRes.rows[0].event_id, orgId, 404);
    await db.delete(id);
    return Promise.resolve();
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getList = async (query, orgId) => {
  try {
    let { eventId } = query;
    if (!eventId || parseInt(eventId) != eventId)
      throw { code: 400, message: 'query parameter eventId is required (number)' }
    await checkAccess(eventId, orgId, 403);
    let getQueryRes = await db.getList(eventId);
    let judges = getQueryRes.rows.map(outputMapper);
    return judges;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.getById = async (id, orgId) => {
  try {
    if (parseInt(id) != id)
      throw { code: 404 }
    let getQueryRes = await db.get(id);
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }
    let judge = getQueryRes.rows.map(outputMapper)[0];
    await checkAccess(judge.eventId, orgId, 404)
    return judge;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.invite = async (id, query, orgId) => {
  // TODO: add logic for transactions between other statuses
  try {
    let reinvite = query.reinvite == 'true'

    if (parseInt(id) != id)
      throw { code: 404 }

    let getQueryRes = await db.get(id);
    let judge = getQueryRes.rows.map(outputMapper)[0];
    if (getQueryRes.rows.length === 0)
      throw { code: 404 }

    let event = await checkAccess(judge.eventId, orgId, 404)

    if (judge.status != 'created' && !reinvite)
      return judge;

    if (judge.status == 'created')
      await db.updateStatus(id, 'invited')

    // send invitation letter async
    emailer.sendMail(judge.email, {
      eventTitle: event.title,
      name: judge.name,
      link: judge.judgingLink
    })

    return unit.getById(id, orgId);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.inviteAll = async (id, body, query, orgId) => {
  try {
    await validate(body, inviteSchema)
    let eventId = body.eventId;
    let reinvite = query.reinvite == 'true';

    let event = await checkAccess(eventId, orgId, 403)
    let judges = await unit.getList({ eventId }, orgId);

    // send invitation letter to all judges of the event
    judges.forEach(j => {
      if (j.status != 'created' && !reinvite)
        return;
      emailer.sendMail(j.email, {
        eventTitle: event.title,
        name: j.name,
        link: j.judgingLink
      })
      j.status = 'invited';
    })

    await db.updateStatus(null, 'invited', eventId)
    return judges;
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

unit.count = async (eventId, judgeType) => {
  try {
    return db.count(eventId, judgeType);
  } catch (err) {
    return Promise.reject({ code: err.code, message: err.message });
  }
}

module.exports = unit;
const express = require('express');
const router = express.Router();
const esb = require('../ESBs/orgs.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');

router.use(authN.middleware());

router.get('/me', function (req, res) {
  esb.getById(req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});
  
router.post('/token', async (req, res) => {
  try {
    const result = await esb.setToken(req._acl.id, req.body.code);
    res.send(result);
  } catch (err) {
    errorHandler(err, res);
  }
});

module.exports = router;
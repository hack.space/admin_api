const express = require('express');
const router = express.Router();
const esb = require('../ESBs/messages.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');

router.use(authN.middleware());

router.post('/', function (req, res) {
  esb.post(req.body, req._acl.orgId)
    .then(() => {
      res.send();
    })
    .catch(err => errorHandler(err, res));
});

router.get('/', function (req, res) {
  esb.getList(req.query, req._acl.orgId)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

module.exports = router
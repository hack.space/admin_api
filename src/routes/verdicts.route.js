const express = require('express');
const router = express.Router();
const esb = require('../ESBs/verdicts.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');
const authZ = require('../lib/authZ');

router.use(authN.middleware());
router.use(authZ.middleware());

router.get('/', function (req, res) {
  esb.getList(req.query, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.put('/list', function (req, res) {
  esb.updateList(req.body, req._acl.id, req._acl.eventId, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
})

router.put('/:id', function (req, res) {
  esb.update(req.body, req.params.id, req._acl.id, req._acl.eventId, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.get('/:id', function (req, res) {
  let teamId = req.params.id;
  esb.getById(teamId, req._acl.id, req._acl.eventId, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.get('/:id/comments', function (req, res) {
  let teamId = req.params.id;
  esb.getCommentsByTeam(teamId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});
// TODO: router.del('/:id/comment'); - delete comment

router.post('/:eventId/export', async (req, res) => {
  try {
    const result = await esb.exportResults(req.params.eventId, req._acl.id, req._acl.orgId);
    res.send(result);
  } catch (err) {
    errorHandler(err, res);
  }
}); 

module.exports = router;
const express = require('express');
const router = express.Router();
const esb = require('../ESBs/login.esb');
const errorHandler = require('../lib/errorHandler');

router.post('/', function (req, res) {
  esb.login(req.body.login, req.body.pwd)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
}); 

module.exports = router;
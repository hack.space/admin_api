const express = require('express');
const router = express.Router();
const esb = require('../ESBs/participants.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');
const authZ = require('../lib/authZ');

router.use(authN.middleware());
router.use(authZ.middleware());

/**
 * GET /participants/:id
 * @param {number} id - eventId
 */
router.get('/:id', authN.middleware(), function (req, res) {
  esb.getList(req.params.id, req._acl.orgId)
    .then(prts => {
      res.send(prts);
    })
    .catch(err => errorHandler(err, res));
});

router.post('/:eventId/:hackerId/confirm', authN.middleware(), (req, res) => {
  const { hackerId, eventId } = req.params;
  esb.updateParticipationStatus(eventId, hackerId, req._acl.orgId, 'confirmed')
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.post('/:eventId/:hackerId/decline', authN.middleware(), (req, res) => {
  const { hackerId, eventId } = req.params;
  esb.updateParticipationStatus(eventId, hackerId, req._acl.orgId, 'declined')
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

router.post('/:eventId/release-on-hold', authN.middleware(), (req, res) => {
  const { eventId } = req.params;
  esb.releaseOnHold(eventId, req._acl.orgId)
    .then(result => {
      res.send(result);
    })
    .catch(err => errorHandler(err, res));
});

module.exports = router;
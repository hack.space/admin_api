const express = require('express');
const router = express.Router();
const esb = require('../ESBs/export.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');
const authZ = require('../lib/authZ');

router.use(authN.middleware());
router.use(authZ.middleware());

router.post('/:eventId', async (req, res) => {
  try {
    const result = await esb.exportResults(Number(req.params.eventId), req._acl.id, req._acl.orgId);
    res.send(result);
  } catch (err) {
    errorHandler(err, res);
  }
}); 

module.exports = router;
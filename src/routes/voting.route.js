const express = require('express');
const router = express.Router();
const esb = require('../ESBs/voting.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');

router.get('/start', authN.middleware(), function (req, res) {
  console.debug(req._acl);
  esb.start(req._acl.id, req._acl.eventId, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.post('/start/:hash', function (req, res) {
  esb.pregenerateAndStart(req.params.hash)
    .then(redirectLink => {
      const token = redirectLink.split('/').pop()
      res.json({token});
    }).catch(err => errorHandler(err, res));
})

// alternative call for QR
router.get('/start/:hash', function (req, res) {
  esb.pregenerateAndStart(req.params.hash)
    .then(redirectLink => {
      res.redirect(redirectLink)
    }).catch(err => errorHandler(err, res));
})

module.exports = router;

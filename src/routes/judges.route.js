const express = require('express');
const router = express.Router();
const esb = require('../ESBs/judges.esb');
const errorHandler = require('../lib/errorHandler');
const authN = require('../lib/authN');
const authZ = require('../lib/authZ');

router.use(authN.middleware());
router.use(authZ.middleware());

router.get('/', function (req, res) {
  esb.getList(req.query, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.post('/', function (req, res) {
  esb.create(req.body, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.get('/:id', function (req, res) {
  esb.getById(req.params.id, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.put('/:id', function (req, res) {
  esb.update(req.params.id, req.body, req._acl.orgId)
    .then(result => {
      res.send(result);
    }).catch(err => errorHandler(err, res));
});

router.delete('/:id', function (req, res) {
  esb.delete(req.params.id, req._acl.orgId)
    .then(result => {
      res.status(200).send();
    }).catch(err => errorHandler(err, res));
});

router.post('/:id/invite', function (req, res) {
  if (req.params.id === 'all')
    esb.inviteAll(req.params.id, req.body, req.query, req._acl.orgId)
      .then(result => {
        res.send(result);
      }).catch(err => errorHandler(err, res));
  else
    esb.invite(req.params.id, req.query, req._acl.orgId)
      .then(result => {
        res.send(result);
      }).catch(err => errorHandler(err, res));
})


module.exports = router;
const { Pool } = require('pg');
const pool = new Pool();
const fs = require('fs');

let handleError = function (err) {
  switch (err.code) {
    case "23505": // duplicate of uniq key
      return Promise.reject({
        code: 409, // Conflict
      });
    case "23503":
      return Promise.reject({
        code: 400,
      });
    default:
      return Promise.reject({
        code: 500,
        message: err.message || "db error"
      });
  }
}

// the pool with emit an error on behalf of any idle clients
// it contains if a backend error or network partition happens
pool.on('error', (err, client) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
});

// promise - checkout a client
pool.connect()
  .then(client => client.release())
  .catch(err => {
    console.log('failed connection to db', err);
    process.exit(-1);
  })

/**
 * @deprecated use call instead
 */
exports.pool = pool;
/**
 * @deprecated use call instead
 */
exports.handleError = handleError;

exports.call = function (query) {
  return pool.query(query)
    .catch(handleError);
}
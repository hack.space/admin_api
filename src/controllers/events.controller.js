const { call } = require('../pg-util');

let db = {};

// converts API object keys to DB object keys, the same if not mentioned
let dictDbKey = {
  "checkInLocation": "location",
}
// map API object's values to DB flat value (string, number, etc.)
let dictDbValue = {
  "checkInLocation": (v) => `lat:${v.lat},lng:${v.lng}`
}

/**
 * Generates String for parametrs in SQL:
 * @param {number} i - number of elements
 * @param {number?} startK - index of first element
 * @example
 * input: i = 3, startK = 3
 * output: "$3,$4,$5" 
 */
function numLine(i, startK = 2) {
  let k = startK;
  let str = '';
  while ((k - startK) !== i) { str += `$${k++},` }
  str = str.slice(0, -1); // removes coma in the end of string
  return str;
}

db.create = function (obj, orgId) {
  let attrs = Object.keys(obj).map(k => (dictDbKey[k] || k).toLowerCase());
  let indxs = numLine(attrs.length);
  let values = Object.keys(obj).map(k =>
    typeof dictDbValue[k] === "function" ? dictDbValue[k](obj[k]) : obj[k]);

  let text = `INSERT INTO events (organizationId,${attrs}) values ($1,${indxs})
  RETURNING id`;

  let query = {
    name: 'create-event',
    text,
    values: [orgId, ...values]
  }

  return call(query);
}

db.getById = function (eventId, orgId) {
  let text = `select * from events where id = $1 and organizationId = $2`;

  let query = {
    name: 'get-event-by-id',
    text,
    values: [eventId, orgId]
  }

  return call(query);
}

db.getList = function (orgId) {
  let text = `select e.*, count(hackerId) as "participants"
  from events e
  left join participations p 
  on e.id = p.eventId
  where organizationId = $1
  group by e.id`;

  let query = {
    name: '',
    text,
    values: [orgId]
  }

  return call(query);
}

db.update = function ({ id, ...obj }, eventId, orgId) {
  let attrs = Object.keys(obj).map(k => (dictDbKey[k] || k).toLowerCase());
  const N = attrs.length;
  let indxs = numLine(N, 1);
  let values = Object.keys(obj).map(k =>
    typeof dictDbValue[k] === "function" ? dictDbValue[k](obj[k]) : obj[k]);

  let setClause = N === 1
    ? `set ${attrs[0]} = $1`
    : `set (${attrs}) = (${indxs})`

  let text = `update events 
  ${setClause}
  where id = $${N + 1} and organizationId = $${N + 2}`;

  let query = {
    text,
    values: [...values, eventId, orgId]
  }

  return call(query);
}

db.delete = function (eventId, orgId) {
  let text = `delete from events where id = $1 and organizationId = $2`;

  let query = {
    name: 'delete-event',
    text,
    values: [eventId, orgId]
  }

  return call(query);
}

/**
 * Updpates judging_enabled field.
 * @param {string|number} eventId 
 * @param {string|number} orgId 
 * @param {boolean} value 
 */
db.updateJudgingEnabled = function (eventId, orgId, value) {
  let text = `update events set judging_enabled = $1
  where id = $2 and organizationId= $3`

  let query = {
    name: 'update-event-judging-enabled',
    text,
    values: [value, eventId, orgId]
  }

  return call(query);
}

db.getByHash = function (hash) {
  let text = `select * from events where vote_hash = $1`;

  let query = {
    name: 'get-event-by-hash',
    text,
    values: [hash]
  }

  return call(query);
}

module.exports = db;
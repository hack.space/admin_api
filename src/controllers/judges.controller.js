const { call } = require('../pg-util');

let db = {}

let JUDGE_STATUS = {
  created: 'created',
  invited: 'invited',
  activated: 'activated',
  completed: 'completed'
}

const JUDGE_TYPES = {
  jury: 0,
  participant: 1
}

db.insert = (judge, status = JUDGE_STATUS.created) => {
  if (!JUDGE_STATUS[status])
    status = JUDGE_STATUS.created;

  if (Object.values(JUDGE_TYPES).indexOf(judge.type) < 0)
    judge.type = JUDGE_TYPES.jury;

  let text = /*sql*/`
    INSERT INTO Judges
    (name,email,token,event_id,status,type)
    VALUES ($1,$2,$3,$4,$5,$6)
    RETURNING id
  ;`

  let query = {
    name: 'insert-judge',
    text,
    values: [judge.name, judge.email, judge.token, judge.eventId, status, judge.type]
  }

  return call(query)
}

db.get = (id) => {
  let text = `SELECT * FROM Judges
  WHERE id = $1`
  let query = {
    name: 'select-judge-by-id',
    text,
    values: [id]
  }
  return call(query)
}

db.getList = (eventId) => {
  let text = `SELECT * FROM Judges
  WHERE event_id = $1`
  let query = {
    name: 'select-judges',
    text,
    values: [eventId]
  }
  return call(query)
}

db.update = (obj) => {
  let text = `UPDATE Judges
  SET (name,email,event_id,token) = ($1,$2,$3,$4)
  WHERE id = $5`
  let query = {
    name: 'update-judge',
    text,
    values: [obj.name, obj.email, obj.eventId, obj.token, obj.id]
  }
  return call(query)
}

/**
 * Updates status of judges.
 * @param {number} id of a judge
 * @param {string} status one of listed in STATUSES 
 * @param {number?} eventId id of an event. If set, then statuses
 * of all judges of the event will be updated.
 */
db.updateStatus = (id, status, eventId) => {
  if (!JUDGE_STATUS[status])
    status = 'created';

  let text = `UPDATE Judges
  SET status = $1
  WHERE ${eventId ? 'event_id' : 'id'} = $2`

  let query = {
    name: `update-judge-status`,
    text,
    values: [status, eventId || id]
  }

  return call(query)
}

db.delete = (id) => {
  let text = `DELETE FROM Judges
  WHERE id = $1`
  let query = {
    name: 'delete-judge',
    text,
    values: [id]
  }
  return call(query)
}

db.count = async (eventId, type) => {
  let values = [eventId]

  let text = /*sql*/`
    select count(id) from judges
    where event_id = $1
  `

  if (!JUDGE_TYPES[type]) {
    text += /*sql*/`and type = $2;`
    values.push(type)
  }

  const query = {
    text,
    values
  }

  return call(query)
    .then(res => {
      return +res.rows[0].count
    });
}


module.exports = db;
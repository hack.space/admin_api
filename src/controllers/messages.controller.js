const { call } = require('../pg-util');
const Promise = require('promise');
const _ = require('underscore');

let db = {};

db.getMessagesList = function (eventId, lastMessageTimestamp) {
  let text = `SELECT id,eventId,posttimestamp,content FROM Messages m
  WHERE postTimestamp > $1 and eventId = $2`;

  let query = {
    name: 'get-messages',
    text,
    values: [lastMessageTimestamp, eventId]
  };

  return call(query);
}

/**
 * Selects tgIds of hackers
 * @param {string} eventId - id of event
 * TODO: make it filterable by status or other parameters
 */
db.getReceivers = function (eventId) {
  let text = `SELECT tgId FROM Participations p
  LEFT JOIN Hackers h on p.hackerId = h.id
  WHERE p.eventId = $1 and
  (p.hackerStatus = 'applied' or p.hackerStatus = 'confirmed' or p.hackerStatus = 'activated')`;

  let query = {
    name: 'get-receivers',
    text,
    values: [eventId]
  }

  return call(query);
}

db.createMessage = function (eventId, content) {
  let text = `INSERT INTO Messages (eventId, content, postTimestamp)
  VALUES ($1, $2, (select extract(epoch from now())));`

  let query = {
    name: 'insert-message',
    text,
    values: [eventId, content]
  }

  return call(query);
}

module.exports = db;
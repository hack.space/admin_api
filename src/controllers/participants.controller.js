const { call } = require('../pg-util');
const Promise = require('promise');

let db = {};

db.getList = (eventId) => {
  let text = `
  SELECT g.*, min(p.hackerStatus) as "status", bool_or(p.isSearchable),
  COALESCE(json_agg(s.tag) FILTER (WHERE s.tag IS NOT NULL), '[]') as "skills",
  min(t.name) as "teamName"
  FROM (
    SELECT h.id, h.username, h.pic, h.contactPhone, h.email, h.tgProfileLink, h.slackdeeplink, h.bio, h.slackid,
    coalesce(sum(p.xp),0) as xp
    FROM Participations p
    LEFT JOIN Hackers h on h.id = p.hackerId
    GROUP BY h.id
  ) g
  LEFT JOIN Participations p on g.id = p.hackerid
  LEFT JOIN HackerSkills hs on hs.hackerId = g.id
  LEFT JOIN Skills s on s.id = hs.skillId
  LEFT JOIN Teams t on t.id = p.team_id
  WHERE eventId = $1
  GROUP BY g.id, g.username, g.pic, g.contactPhone, g.email, g.tgProfileLink, g.xp, g.slackdeeplink, g.bio, g.slackid;`

  let query = {
    name: 'select-participants',
    text,
    values: [eventId]
  }

  return call(query);
}

db.getParticipationStatus = (eventId, hackerId) => {
  const text = `SELECT hackerStatus FROM participations WHERE HackerId = $1 and EventId = $2;`;
  const query = {
    name: 'select-participation-status',
    text,
    values: [hackerId, eventId],
  }
  return call(query).then(({ rows }) => rows[0] ? rows[0].hackerstatus : null);
}

db.updateStatusOnHold = (eventId, hackerId, status) => {
  const query = {
    name: 'update-status-on-hold',
    text: `UPDATE participations SET status_on_hold = $1 WHERE eventId = $2 and hackerId = $3;`,
    values: [status, eventId, hackerId],
  }
  return call(query);
}

db.setStatusesFromOnHold = (eventId) => {
  const query = {
    name: 'set-statuses-from-on-hold',
    text: `UPDATE participations SET hackerStatus = status_on_hold WHERE eventId = $1 and hackerStatus = 'applied' and status_on_hold != '';`,
    values: [eventId],
  }
  return call(query);
}

module.exports = db;
const { call } = require('../pg-util');

let db = {}

db.insert = function (obj) {
  let text = `INSERT INTO Teams 
  (name, speech_order, event_id)
  VALUES ($1,$2,$3)
  RETURNING id;`

  let query = {
    name: 'insert-team',
    text,
    values: [obj.name, obj.order, obj.eventId]
  }

  return call(query);
}

db.get = function (id) {
  let text = `SELECT * FROM Teams
  WHERE id = $1;`

  let query = {
    name: 'select-team-by-id',
    text,
    values: [id]
  }

  return call(query)
}

db.getList = function (eventId) {
  let text = `
  select t2.*, g.members from
  (SELECT t.id, COALESCE(json_agg(h.username) FILTER (WHERE h.id IS NOT NULL), '[]') as "members" FROM Teams t
    left join participations p on p.team_id = t.id
    left join hackers h on p.hackerid = h.id
  WHERE event_id = $1
  group by t.id
  ) g
  left join teams t2 on g.id = t2.id
  ORDER BY speech_order, name;`

  let query = {
    name: 'select-teams-members',
    text,
    values: [eventId]
  }

  return call(query);
}

db.update = function (obj) {
  let text = `UPDATE Teams
  SET (name,speech_order,event_id) = ($1,$2,$3)
  WHERE id = $4`

  let query = {
    name: "update-team",
    text,
    values: [obj.name, obj.order, obj.eventId, obj.id]
  }

  return call(query)
}

db.delete = function (id) {
  let text = `DELETE FROM Teams
  WHERE id = $1`

  let query = {
    name: 'delete-team',
    text,
    values: [id]
  }

  return call(query)
}

module.exports = db;
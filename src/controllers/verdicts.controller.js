const { call } = require('../pg-util');

let db = {}

call(
  /*sql*/`
  CREATE OR REPLACE FUNCTION exec_criterias(_team_id int = NULL, _event_id int = NULL)
  RETURNS json AS
  $func$
    SELECT coalesce(array_to_json(array_agg(c)), '[]') as criterias 
    FROM (
      select c.id, c.name, c.scale_from as "scaleFrom", c.scale_to as "scaleTo", round(coalesce(sum(v.score),0),4) as "sum", 
        round(coalesce(avg(v.score),0),4) as "average" from criterias c
      left join verdicts v on v.criteria_id = c.id
      full join teams t on t.id = v.team_id
      where t.id = _team_id and c.event_id = _event_id
      group by c.id
    ) c
  $func$ LANGUAGE sql;
  `
)

call(
  /*sql*/`
  CREATE OR REPLACE FUNCTION average_weighted(_team_id int = NULL, _event_id int = NULL, _judge_type int = 0)
  RETURNS numeric(5,3) AS
  $func$
    SELECT sum(average*weight) as average_weighted 
    FROM (
      select c.id, c.name, c.weight, c.scale_from as "scaleFrom", c.scale_to as "scaleTo", round(coalesce(sum(v.score),0),4) as "sum", 
        round(coalesce(avg(v.score),0),4) as "average" from criterias c
      left join verdicts v on v.criteria_id = c.id
      left join judges j on v.judge_id = j.id
      full join teams t on t.id = v.team_id
      where t.id = _team_id and c.event_id = _event_id and j.type = _judge_type
      group by c.id
    ) c
  $func$ LANGUAGE sql;
  `
)

call(
  /*sql*/`
  CREATE OR REPLACE FUNCTION exec_judge_progress(_team_id int = NULL, _event_id int = NULL, _judge_id int = NULL)
  RETURNS json AS
  $func$
    SELECT coalesce(array_to_json(array_agg(c)), '[]') as criterias 
    FROM (
      select c.id, c.name, c.description, v.score as "points", c.poly_title as "polyTitle", c.scale_from as "scaleFrom", c.scale_to as "scaleTo" from criterias c
      left join verdicts v on v.criteria_id = c.id
      full join teams t on t.id = v.team_id
      where t.id = _team_id and c.event_id = _event_id and v.judge_id = _judge_id
    ) c
  $func$ LANGUAGE sql;
  `
)

call(
  /*sql*/`
  CREATE EXTENSION IF NOT EXISTS tablefunc;
  `
);

db.get = function (teamId, judgeId, eventId) {
  let text = /*sql*/`
    SELECT t.id, t.name, t.speech_order, 
    exec_judge_progress(t.id, $1, $2) as "criterias"
    FROM teams t
    left join verdicts v on t.id = v.team_id
    where t.event_id = $3 and t.id = $4
    group by t.id
  ;`

  let query = {
    name: 'select-verdict-by-id',
    text,
    values: [eventId, judgeId, eventId, teamId]
  }

  return call(query)
}

db.getList = function (eventId, p2p = false) {
  let text = /*sql*/`
    SELECT t.id, t.name, t.speech_order, 
    round(coalesce(sum(score),0),4) as "sum", 
    round(coalesce(avg(score),0),4) as "average",
    average_weighted(t.id, $1),
    ${p2p ? 'average_weighted(t.id, $1, 1) as "average_weighted_peers",' : ''}
    exec_criterias(t.id, $1) as "criterias"
    FROM teams t
    inner join verdicts v on t.id = v.team_id
    inner join criterias c on c.id = v.criteria_id
    where t.event_id = $1
    group by t.id
  ;`

  let query = {
    name: `select-verdicts.p2p-${p2p}`,
    text,
    values: [eventId]
  }

  return call(query);
}

db.update = function (judgeId, teamId, criteriaId, score) {
  console.log(`verdict ${Date.now()} j:${judgeId} t:${teamId} c:${criteriaId} s:${score}`)
  
  let text = /*sql*/`
    INSERT INTO Verdicts (judge_id, team_id, criteria_id, score)
    VALUES ($2,$3,$4,$1)
    ON CONFLICT (judge_id, team_id, criteria_id)
    DO UPDATE SET score = $1
  ;`

  let query = {
    name: "update-verdict",
    text,
    values: [score, judgeId, teamId, criteriaId]
  }

  return call(query)
}

/**
 * Is used in export only.
 */
db.getByJudge = function (judgeId) {
  // TODO: Expand to use for more than 3 criteria
  const text = /*sql*/`
    SELECT * FROM crosstab(
      'select t.name, criteria_id, score from verdicts v left join teams t on v.team_id = t.id where v.judge_id = ${judgeId} order by 1,2') 
    AS ct(team text, criteria_1 int, criteria_2 int, criteria_3 int, criteria_4 int, criteria_5 int);
  ;`;

  const query = {
    text,
  }

  return call(query);
}

module.exports = db;
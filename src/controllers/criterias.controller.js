const { call } = require('../pg-util');

let db = {}

db.insert = function (obj) {
  let text = `INSERT INTO Criterias 
  (name, description, scale_from, scale_to, event_id, weight)
  VALUES ($1,$2,$3,$4,$5,$6)
  RETURNING id;`

  let query = {
    name: 'insert-criteria',
    text,
    values: [obj.name, obj.description, obj.scaleFrom, obj.scaleTo, obj.eventId, obj.weight]
  }

  return call(query);
}

db.get = function (id) {
  let text = `SELECT * FROM Criterias
  WHERE id = $1;`

  let query = {
    name: 'select-criteria-by-id',
    text,
    values: [id]
  }

  return call(query)
}

db.getList = function (eventId) {
  let text = `SELECT * FROM Criterias
  WHERE event_id = $1;`

  let query = {
    name: 'select-criterias',
    text,
    values: [eventId]
  }

  return call(query);
}

db.update = function (obj) {
  let text = `UPDATE Criterias
  SET (name,description,scale_from,scale_to,event_id,weight) = ($1,$2,$3,$4,$5,$6)
  WHERE id = $7`

  let query = {
    name: "update-criteria",
    text,
    values: [obj.name, obj.description, obj.scaleFrom, obj.scaleTo, obj.eventId, obj.weight, obj.id]
  }

  return call(query)
}

db.delete = function (id) {
  let text = `DELETE FROM Criterias
  WHERE id = $1`

  let query = {
    name: 'delete-criteria',
    text,
    values: [id]
  }

  return call(query)
}

module.exports = db;
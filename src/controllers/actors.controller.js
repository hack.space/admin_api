const generatePasswordHash = require('../lib/generatePasswordHash');
const { handleError, pool } = require('../pg-util');

let db = {};

function call(query) {
  return pool.query(query)
    .catch(handleError);
}

db.getByEmail = function (email) {
  let text = `SELECT * FROM OrgActors
  where lower(email) = lower($1)`;

  let query = {
    name: 'get-org-actor-by-email',
    text,
    values: [email]
  }

  return call(query);
}

/**
 * Fetches actor's object by id.
 * @param {string} id of an actor
 */
db.getById = function (actorId, orgId) {
  let text = `SELECT * FROM OrgActors
  where id = $1 and organizationId = $2`;

  let query = {
    name: 'get-org-actor-by-id',
    text,
    values: [actorId, orgId]
  }

  return call(query);
}

db.getList = function (orgId) {
  let text = `SELECT id, email, username, accessLevel
  FROM OrgActors WHERE organizationId = $1`;

  let query = {
    name: 'get-org-actors',
    text,
    values: [orgId]
  }

  return call(query);
}

db.create = function (obj, orgId) {
  var pwdHash = '', salt = '';
  // TODO: это не должен контроллер делать -> перенести в esb
  var { pwdHash, salt } = generatePasswordHash(obj.pwd);

  // TODO: if new_email => update new_email only, otherwise don't touch mails
  // now updating both, because of uniq and not null of new_email
  let text = `INSERT INTO OrgActors (pwd, salt, username, accessLevel, organizationId, email, new_email)
    VALUES ($1, $2, $3, $4, $5, lower($6), lower($7))
    RETURNING id;`;

  let query = {
    name: 'add-org-actor',
    text,
    values: [pwdHash, salt, obj.username, obj.role, orgId, obj.email, obj.email]
  };

  return call(query);
}

db.delete = function (actorId, orgId) {
  let text = `DELETE FROM OrgActors WHERE id = $1 and organizationId = $2`;
  let query = {
    name: 'delete-org-actor',
    text,
    values: [actorId, orgId]
  }
  return call(query);
}

/**
 * Updates row in OrgActors table
 * @param {number} actorId of a row
 * @param {object} obj new row
 * @param {number} orgId - organization id
 */
db.update = function (actorId, obj, orgId) {
  // TODO: if new_email => update new_email only, otherwise don't touch mails
  // now updating both, because of uniq and not null of new_email
  let values = [obj.email, obj.email, obj.role, obj.username]
  if (obj.pwd) {
    let { salt, pwdHash } = generatePasswordHash(obj.pwd);
    values.push(pwdHash, salt);
  }
  values.push(actorId, orgId);

  let text = `update OrgActors set
  (new_email,email, accessLevel, username ${obj.pwd ? ',pwd,salt' : ''})
  = (lower($1),lower($2),$3,$4${obj.pwd ? ',$5,$6' : ''})
  where id = ${obj.pwd ? '$7' : '$5'} and organizationId = ${obj.pwd ? '$8' : '$6'}`; // change it!!!

  let query = {
    name: 'update-actor',
    text,
    values
  }
  return call(query);
}

module.exports = db;
const { call } = require('../pg-util');

let db = {};

db.update = function (judgeId, teamId, comment) {
  console.log(`comment ${Date.now()} j:${judgeId}\t t:${teamId}\t comment:${comment}`)

  const text = /*sql*/`
    INSERT INTO JudgeComments (judge_id, team_id, comment)
    VALUES ($1,$2,$3)
    ON CONFLICT (judge_id, team_id)
    DO UPDATE SET comment = $3
  ;`

  const query = {
    name: "insert-comment",
    text,
    values: [judgeId, teamId, comment]
  }

  return call(query)
}

db.get = function (judgeId, teamId) {
  const text = /*sql*/`
    SELECT * FROM JudgeComments 
    WHERE judge_id = $1 and team_id = $2
  ;`

  const query = {
    name: 'select-judge-comment-by-id',
    text,
    values: [judgeId, teamId]
  }

  return call(query)
}

db.getList = function (teamId) {
  const text = /*sql*/`
    SELECT j.name as judge_name, jc.judge_id, jc.team_id, jc.comment FROM JudgeComments jc
    LEFT JOIN Judges j on jc.judge_id = j.id
    WHERE jc.team_id = $1
  ;`

  const query = {
    name: 'select-judge-comments-list-by-team-id',
    text,
    values: [teamId]
  }

  return call(query);
}

module.exports = db;
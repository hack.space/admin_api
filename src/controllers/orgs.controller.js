const { call } = require('../pg-util');
const Promise = require('promise');

let db = {};

db.getById = (id) => {
  let text = `SELECT * FROM organizations
  WHERE id = $1`;

  let query = {
    name: "get-org-by-id",
    text,
    values: [id]
  }

  return call(query);
}

db.create = (obj) => {
  let text = `INSERT INTO Organizations (title, website, logo)
  VALUES ($1,$2,$3)
  RETURNING id`

  let query = {
    name: 'create-org',
    text,
    values: [obj.title, obj.website, obj.logo]
  }

  return call(query);
}

/**
 * id - orgactor id
 * token - google token
 */
db.setGoogleToken = (id, token) => call({
  name: 'set-google-token',
  text: `update orgactors set google_token = $1 where id = $2`,
  values: [token, id],
});

/**
 * returns {google_token: string}
 */
db.getGoogleToken = (id) => call({
  name: 'get-google-token',
  text: 'select google_token from orgactors where id = $1',
  values: [id],
}).then(({rows}) => rows[0]);

module.exports = db;
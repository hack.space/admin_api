const ENV = process.env.ENV;

const isProd = ENV === 'prod' || ENV === 'production';

let config = {
  JUDGE_BASE_URL: process.env.JUDGE_BASE_URL,
  url: isProd ? 'https://hackspace.pro' : 'https://hackspace.pw',
}

if (isProd) {
  console.debug = () => {};
  console.log("Running in Production environment")
}
else { // dev by default
  console.warn("Running in Development environment")
}

module.exports = config
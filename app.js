const express = require('express');
const app = express();
const glob = require('glob');
const { join } = require('path');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const {isProd} = require('./src/config');

if (isProd) {
  // app.use(helmet());
} else {
  // app.use(cors());
}

app.use(bodyParser.json());

app.use(function(req,res,next){
  if (req.method === 'OPTIONS')
    res.send(200);
  else
    next();
});

app.get('/api/admin/docs', function (req,res) {
  res.sendFile(join(__dirname, 'apidoc', 'api.html'))
});

// Plug-in routes.
glob('src/routes/*.js', (err, pathes) => {
  if (err) {
    console.log('failed glob', err);
    process.exit(-1);
  }
  for (let path of pathes)
    app.use(`/api/admin/${/\/(\w+)\./.exec(path)[1]}`, require(`./${path}`));
});

const PORT = process.env.PORT || 8082;
app.listen(PORT, err => {
  if (!err)
    console.log('Service is listening on port ' + PORT);
  else 
    process.exit(-1);
});

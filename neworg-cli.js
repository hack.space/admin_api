// TODO: generate pwd automatically and send via email to admin

const readline = require('readline')
const actorDb = require('./src/controllers/actors.controller')
const orgDb = require('./src/controllers/orgs.controller')

let title, logo, website, adminUsername, adminPwd, adminEmail

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question('Org Title > ', async (answer) => {
  title = answer
  rl.question('Org logo (link) > ', (answer) => {
    logo = answer
    rl.question('Org website > ', (answer) => {
      website = answer
      rl.question('Admin username > ', (answer) => {
        adminUsername = answer
        rl.question('Admin password > ', (answer) => {
          adminPwd = answer
          rl.question('Admin email > ', (answer) => {
            adminEmail = answer
            rl.close()
            createOrg()
              .then(createAdmin)
              .then(() => process.exit(1));
          })
        })
      })
    })
  })
})

async function createOrg() {
  try {
    let createQueryRes = await orgDb.create({
      title,
      logo,
      website
    })
    let id = createQueryRes.rows[0].id
    console.log('Organization has been create with id: ' + id)
    return id
  } catch (err) {
    console.log(err.message)
  }
}

async function createAdmin(orgId) {
  try {
    let createQueryRes = await actorDb.create({
      username: adminUsername,
      role: "admin",
      email: adminEmail,
      pwd: adminPwd
    }, orgId)
    let id = createQueryRes.rows[0].id
    console.log('Admin has been created with id: ' + id)
  } catch (err) {
    console.log(err.message);
  }
}